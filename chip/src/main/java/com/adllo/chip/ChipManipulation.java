package com.adllo.chip;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.android.flexbox.FlexboxLayout;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by dev_three_adllo on 2/20/18.
 */

public class ChipManipulation extends LinearLayout {
    private Context mContext;
    private EditText mEditText;
    private TextView mTextView;
    private FlexboxLayout flexLayout;
    private int indexCount = 0;
    private String chipBackgroundColor;
    private String chipTextColor;
    private String chipTextSize;
    private String chipTextStyle;
    private String chipHint;
    private String chipType = "hashtag";
    private String chipSymbol;
    private String editTextMaxLength;
    private String editTextColor;

    public ChipManipulation(Context context) {
        super(context);
    }

    public ChipManipulation(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public ChipManipulation(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    public ChipManipulation(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        mContext = context;

        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        LinearLayout linearLayout = (LinearLayout) layoutInflater.inflate(R.layout.flexbox_template, this, true);

        flexLayout = (FlexboxLayout) linearLayout.findViewById(R.id.divFlex);
        mEditText = (EditText) linearLayout.findViewById(R.id.editText);

        mEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                charSequence.toString();

                if (charSequence.length() > 0) {
                    String keyCode = charSequence.toString().substring(charSequence.length() - 1);

                    if (keyCode.equals(" ")) {
                        setText();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.chip_drawable);
        chipBackgroundColor = a.getString(R.styleable.chip_drawable_background_color);
        chipTextColor = a.getString(R.styleable.chip_drawable_text_color);
        chipTextSize = a.getString(R.styleable.chip_drawable_text_size);
        chipTextStyle = a.getString(R.styleable.chip_drawable_text_style);
        chipHint = a.getString(R.styleable.chip_drawable_hint);
        chipType = a.getString(R.styleable.chip_drawable_type);
        editTextMaxLength = a.getString(R.styleable.chip_drawable_edit_text_max_length);
        editTextColor = a.getString(R.styleable.chip_drawable_edit_text_color);

        setSymbol();

        if (chipHint != null)
        {
            setHint(chipHint);
        }

        if (editTextMaxLength != null)
        {

        }

        if (editTextColor != null)
        {
            setEditTextColor();
        }

        mEditText.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
            if (hasFocus) {
            } else {
                setText();
            }
            }
        });
    }

    private void setEditTextMaxLength()
    {
        Integer maxLength = Integer.parseInt(editTextMaxLength);

        if (maxLength > 0 && maxLength <= 30)
        {
            mEditText.setFilters(new InputFilter[] {
                    new InputFilter.LengthFilter(maxLength)
            });
        }
    }

    private void setEditTextColor()
    {
        mEditText.setTextColor(Color.parseColor(editTextColor));
    }

    private void setText()
    {
        String text = mEditText.getText().toString().trim();

        if (chipType.equals("hashtag"))
        {
            text = applyHashtagRules(text);
        }

        if (chipType.equals("mention"))
        {
            text = applyMentionRules(text);
        }

        if (text != null && !text.isEmpty())
        {
            flexLayout.addView(createNewTextView(text), indexCount++);
            mEditText.setText("");
        }
    }

    private void setSymbol()
    {
        Map<String, String> map = new HashMap<>();

        map.put("hashtag", "#");
        map.put("mention", "@");

        chipSymbol = map.get(chipType);
    }

    private String applyHashtagRules(String text)
    {
        return text.replaceAll("[^a-zA-Z0-9_]", "");
    }

    private String applyMentionRules(String text) {
        text = text.replaceAll("[^a-zA-Z0-9_.]", "");
        int flag = 0;
        String newText = "";


//         remove first dot
        if (text.length() > 0)
        {
            while (true)
            {
                String firstChar = Character.toString(text.charAt(0));

                if (firstChar.equals("."))
                {
                    text = text.substring(1);

                    if (text.length() == 0)
                    {
                        break;
                    }

                    continue;
                }

                break;
            }
        }

//        remove last dot
        if (text.length() > 0)
        {
            while (true)
            {
                String lastChar = Character.toString(text.charAt(text.length() - 1));

                if (lastChar.equals("."))
                {
                    text = text.substring(0, (text.length() - 1));

                    if (text.length() == 0)
                    {
                        break;
                    }

                    continue;
                }

                break;
            }
        }

        char[] removedDotText = text.toCharArray();

        for (int i = 0; i < text.length(); i++)
        {
            if (removedDotText[i] == '.')
            {
                flag = 1;
            }

            if (flag == 1)
            {
                if (removedDotText[i] != '.')
                {
                    newText += "." + removedDotText[i];
                    flag = 0;
                }
            }
            else
            {
                newText += removedDotText[i];
            }
        }

        return newText;
    }

    private void setBackgroundColor(String backgroundColor)
    {
        Resources res = getResources();
        GradientDrawable drawable = (GradientDrawable) res.getDrawable(R.drawable.shape_chip_drawable);
        drawable.setColor(Color.parseColor(backgroundColor));
    }

    private void setTextColor(String textColor)
    {
        mTextView.setTextColor(Color.parseColor(textColor));
    }

    private void setTextSize(String textSize)
    {
        String temp[] = textSize.split("sp");

        mTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, Integer.parseInt(temp[0]));
    }

    private void setTextStyle(String textStyle)
    {
        if (textStyle.equals("bold"))
        {
            mTextView.setTypeface(null, Typeface.BOLD);
        }
        else if (textStyle.equals("italic"))
        {
            mTextView.setTypeface(null, Typeface.ITALIC);
        }
        else if (textStyle.equals("bold_italic"))
        {
            mTextView.setTypeface(null, Typeface.BOLD_ITALIC);
        }
        else if (textStyle.equals("normal"))
        {
            mTextView.setTypeface(null, Typeface.NORMAL);
        }
    }

    public ArrayList<String> getValue()
    {
        ArrayList<String> list = new ArrayList<>();

        for( int i = 0; i < (flexLayout.getChildCount() - 1); i++ ){
            RelativeLayout relativeInflater = (RelativeLayout) flexLayout.getChildAt(i);
            RelativeLayout layout = (RelativeLayout) relativeInflater.getChildAt(0);

            TextView tempText = (TextView) layout.getChildAt(1);

            if (tempText.isShown())
            {
                String[] text = tempText.getText().toString().split(chipSymbol);
                list.add(text[1]);
            }
        }

        return list;
    }

    public void setValue(List<String> arrTemp)
    {
        for (int i = 0; i < (arrTemp.size()); i++ )
        {

            setValue(arrTemp.get(i));
        }
    }

    public void setHint(String hint)
    {
        mEditText.setHint(Html.fromHtml("<i>" + hint + "</i> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;"));
    }

    public void setValue(String data)
    {
        flexLayout.addView(createNewTextView(data), indexCount++);
    }

    public void setValueView(List<String> arrTemp)
    {
        for (int i = 0; i < (arrTemp.size()); i++ )
        {

            setValueView(arrTemp.get(i));
        }
    }

    public void setValueView(String data)
    {
        flexLayout.addView(createNewTextViewChip(data), indexCount++);
    }

    @SuppressLint("ResourceAsColor")
    private RelativeLayout createNewTextView(String text) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        final RelativeLayout relativeInflater = (RelativeLayout) layoutInflater.inflate(R.layout.text_template, null);
        final RelativeLayout relativeLayout = (RelativeLayout) relativeInflater.findViewById(R.id.relative1);

        mTextView = (TextView) relativeLayout.findViewById(R.id.textview1);

        mTextView.setText(chipSymbol + text);

        if (chipBackgroundColor != null)
        {
            setBackgroundColor(chipBackgroundColor);
        }

        if (chipTextColor != null)
        {
            setTextColor(chipTextColor);
        }

        if (chipTextSize != null)
        {
            setTextSize(chipTextSize);
        }

        if (chipTextStyle != null)
        {
            setTextStyle(chipTextStyle);
        }

        relativeLayout.setBackgroundResource(R.drawable.shape_chip_drawable);

        relativeInflater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            relativeInflater.setVisibility(View.GONE);
            }
        });

        return relativeInflater;
    }

    private RelativeLayout createNewTextViewChip(String text) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        final RelativeLayout relativeInflater = (RelativeLayout) layoutInflater.inflate(R.layout.text_template_view, null);
        final RelativeLayout relativeLayout = (RelativeLayout) relativeInflater.findViewById(R.id.relative1);

        mTextView = (TextView) relativeLayout.findViewById(R.id.textview1);

        mTextView.setText(chipSymbol + text);

        if (chipBackgroundColor != null)
        {
            setBackgroundColor(chipBackgroundColor);
        }

        if (chipTextColor != null)
        {
            setTextColor(chipTextColor);
        }

        if (chipTextSize != null)
        {
            setTextSize(chipTextSize);
        }

        if (chipTextStyle != null)
        {
            setTextStyle(chipTextStyle);
        }

        relativeLayout.setBackgroundResource(R.drawable.shape_chip_drawable);

        return relativeInflater;
    }
}
