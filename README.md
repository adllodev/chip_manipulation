# chip_manipulation
Manipulation function for chip

## xml example
```
<com.adllo.chip.ChipManipulation
    xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:chip_attribute="http://schemas.android.com/apk/res-auto"
    android:layout_width="wrap_content"
    android:layout_height="wrap_content"
    android:id="@+id/layoutChip"
    chip_attribute:background_color="#F2A81A"
    chip_attribute:text_color="#6B6861"
    chip_attribute:text_size="16sp"
    chip_attribute:text_style="bold"
    chip_attribute:hint="input chip here!!!"
    chip_attribute:type="hashtag"
    chip_attribute:edit_text_max_length="20"
    chip_attribute:edit_text_color="#FF0000"
/>
```

## attribute
```
background_color : change chip color. only accepts hex value with #
text_color : change text color inside chip. only accepts hex value with #
text_size : change text size inside chip. only accepts sp format ex: 16sp
text_style : change text style inside chip. only accepts bold, italic, bold_italic, normal
hint : set hint text. default: input chip here!
type : set the type on chip. (hashtag to <#> or mention to <@>)
edit_text_max_length : set the max length on edit text with default 30. (between 1 and 30)
edit_text_color: set the color of edit text
```

## method
##### setValue

method to set value into chip, accepts string or array list

ex:
```
chip.setValue(exampleArray);
chip.setValue("exampleString");
```

##### setHint

method to set hint into chip, accepts string

ex:
```
chip.setHint("example");
```

##### getValue

method to get value from chip, return to array of string without the symbol

ex:
```
chip.getValue();
```
return `[example1, example2, example3]`